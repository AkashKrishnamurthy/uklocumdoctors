class AddColorCodeToUsers < ActiveRecord::Migration
  def change
    add_column :users, :colorcode, :string
  end
end
