class AddColumnToProfile < ActiveRecord::Migration
  def change
  	add_column :profiles, :fax, :string
  	add_column :profiles, :organisation, :string
  	add_column :profiles, :telephone_day, :string
  	add_column :profiles, :telephone_night, :string
  	remove_column :profiles, :land_line
  end
end
