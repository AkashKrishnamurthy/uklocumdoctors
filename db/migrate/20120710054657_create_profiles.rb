class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.integer  "user_id"
      t.text     "address"
      t.string   "land_line"
      t.string   "mobile"
      t.text     "other_information"
      t.string   "speciality"
      t.string   "qualification"
      t.datetime "created_at"
      t.datetime "updated_at"
    end
  end
end
