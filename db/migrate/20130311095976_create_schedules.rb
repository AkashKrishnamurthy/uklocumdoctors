class CreateSchedules < ActiveRecord::Migration
  def change
    create_table :schedules do |t|
      t.integer :creator_id
      t.integer :practitioner_id
      t.integer :location_id

      t.datetime :schedule_start
      t.datetime :schedule_end
      t.text :notes

      t.timestamps
    end
  end
end
