class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.integer :practitioner_id
      t.string :name
      t.text :address
      t.string :contact

      t.timestamps
    end
  end
end
