module ApplicationHelper

  def divider
    "<span class='divider'> -> </span>".html_safe
  end
  
  def icon_button(name, icon, class_name)
    "<button class='btn #{class_name}'>  <i class=#{icon}></i> #{name} </button>".html_safe  
  end

  def new_text(resource)
    "<i class='icon-plus-sign'></i> New #{resource}".html_safe  
  end

  def edit_text(resource='')
    "<i class='icon-pencil'></i> Edit #{resource}".html_safe  
  end  

  def date_format(d=nil)
    return "" if d.blank?
    return d.to_date.strftime("%d/%m/%Y").to_s
  end 

  def will_paginate(collection = nil, options = {})
    options[:renderer] = BootstrapPagination::Rails
    super.try :html_safe
  end  

end
