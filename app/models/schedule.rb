class Schedule < ActiveRecord::Base
  attr_accessible :created_by, :location_id, :schedule_start, :notes, :schedule_end, :practitioner_id


  belongs_to :practitioner, :class_name => "User", :foreign_key => "practitioner_id"
  belongs_to :created_by, :class_name => "User", :foreign_key => "creator_id"
  belongs_to :location

  validates :notes, :schedule_start, :schedule_end,  :presence => true

  scope :order_start_asc, -> { order("schedule_start ASC") }
  scope :this_week,  -> {where("schedule_start >= '#{Date.today}'").where("schedule_end <= '#{Date.today + 7}'")}
  delegate :name, :to => :location, :prefix => true, :allow_nil => true

  def self.get_schedules(from, to)
    from_date = from.to_date
    to_date = to.to_date
    self.where("schedule_start >= '#{from_date}'").where("schedule_end <= '#{to_date}'")
  end

  def to_fullcalender
    attribute_hash = {:id => "schedule-#{self.id.to_s}", :title => self.full_description_for_calender, :description => self.full_description_for_calender, :start => schedule_start.iso8601, :end => schedule_end.iso8601, :allDay => 0  }
  end

  def full_description_for_calender
    string = ""
    string += "location: #{self.location_name rescue ''} ," unless  location_id.blank?
    string += "#{self.notes.to_s}"
    return string
  end


  def schedule_from_to
    "#{schedule_start.strftime("%Y-%m-%d %H:%M")} - #{schedule_end.strftime("%Y-%m-%d %H:%M")}"
  end

  def schedule_start_time
    "#{schedule_start.strftime("%d-%m-%Y %H:%M:%S")}"
  end

  def schedule_time
    schedule_start.to_date.strftime("%d/%m/%Y").to_s
  end

  def schedule_end_time
    "#{schedule_end.strftime("%d-%m-%Y %H:%M:%S")}"
  end
end
