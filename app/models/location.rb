class Location < ActiveRecord::Base
  attr_accessible :address, :name, :contact

  validates :name, :contact, :presence => true
end
