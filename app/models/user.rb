class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :authentication_keys => [:username]

  after_create :send_admin_mail
  # Setup accessible (or protected) attributes for your model
  attr_accessible :username, :email, :colorcode, :password, :password_confirmation, :remember_me, :name, :user_roles_attributes, :profile_attributes, :send_sms, :send_email, :staff_default_doctor_selection_attributes, :consultation_fees
  # attr_accessible :title, :body

  has_many :user_roles, :dependent => :destroy
  has_many :roles, :through => :user_roles
  has_one :profile, :dependent => :destroy
  has_many :locations, :foreign_key => "practitioner_id", :dependent => :destroy
  has_many :schedules, :foreign_key => "practitioner_id", :dependent => :destroy
 
  validates :name, :presence => true
  validates :username, :presence => true, :uniqueness => true
  validates :email, :presence => true, :uniqueness => true

  scope :doctors, lambda { joins(:roles).where("roles.name = 'doctor'") }
  scope :staffs, lambda { joins(:roles).where("roles.name = 'nurse'") }
  scope :users, lambda { joins(:roles).where("roles.name = 'user'") }

  accepts_nested_attributes_for :profile, :user_roles, :allow_destroy => :true

  def has_role?(role)
    roles = self.roles.collect(&:name)
    roles.include?(role)
  end

  def self.doctor_list
    self.doctors.map { |doctor| [doctor.name, doctor.id] }
  end

  
  def admin?
    self.has_role?('admin')
  end


  def doctor_or_staff?
    roles = self.roles.collect(&:name)
    return (roles.include?('doctor') || roles.include?('nurse'))
  end
  
  def doctor?
    self.has_role?('doctor')
  end

  def user_role_type
    if admin?
      return 'admin'
    elsif doctor?
      return 'doctor' 
    else
      return 'nurse'
    end
  end

  def staff?
    self.has_role?('nurse')
  end  

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    username = conditions.delete(:username)
    where(conditions).where(["(lower(username) = :value) or lower(email) = :value", { :value => username.downcase }]).first
  end  

 
  
  def doctor_color_code
    colorcode.blank?? "#BB9F9F" : "#{colorcode}"
  end


  def self.send_week_schedules
    User.joins({:user_roles => :role}).where("roles.name != ?", 'admin').where("users.email IS NOT NULL").each do |user|
      unless user.schedules.includes([:location]).this_week.blank?
        Notifier.send_week_schedule_lists(user, user.schedules.includes([:location]).this_week.all).deliver unless user.email.blank?
      end
    end
  end

  def to_param
    "#{id}-#{username}"
  end

  
  


  def send_admin_mail
    Notifier.send_enabled_message(self).deliver_now
  end


end
