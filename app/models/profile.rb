class Profile < ActiveRecord::Base
  attr_accessible :address, :fax, :mobile, :organisation, :telephone_day, :telephone_night,  :other_information, :user_id, :speciality, :qualification
  
  belongs_to :user
  #validates :user_id,  :numericality => true
  validates :mobile, :qualification, :presence => true
end
