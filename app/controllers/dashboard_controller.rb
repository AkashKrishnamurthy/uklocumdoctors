class DashboardController < ApplicationController

  layout 'website'	
  def index
  end

  def settingstandards
  end


  def generalpractitioners
  end

  def doctorinfo
  end

  def nursesinfo
  end

  def aboutus
  end

  def contactus
    if request.post?
      if (!params[:name].blank? && !params[:email].blank? && !params[:message].blank?)
        verify_recaptcha_field_result =  verify_recaptcha
        puts "------verify recaptcha--------#{verify_recaptcha_field_result.inspect}"
        unless verify_recaptcha_field_result
          flash[:notice] = "Word verification response is incorrect"
          render        
        else
          Notifier.send_contactus(params[:name], params[:email], params[:message]).deliver
          flash[:notice] = "Thank you for contacting UK Locum Doctors"
          redirect_to contactus_path
        end
      else
        flash[:notice] = "Please enter all the Details."
        render 
      end      
    end
  end

  def register
  end


end
