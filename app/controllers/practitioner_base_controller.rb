class PractitionerBaseController < ApplicationController
  before_filter :authenticate_user!
  before_filter :find_user
  before_filter :check_user_is_practitioner


  private

  def find_user
    @user = if current_user.doctor_or_staff?
      current_user
    else
  	  User.find(params[:user_id])
    end
  end


  def check_user_is_practitioner
  	if @user.doctor? || @user.staff?
  	  return true
  	else
  	  flash[:notice] = "You don't have a permission."
  	  redirect_to root_path
  	end
  end


end