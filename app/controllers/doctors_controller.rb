class DoctorsController < ApplicationController

  def new
  	@doctor = User.new
  	@doctor.build_profile
  end


  def create
  	@doctor = User.new(params[:user])
  	if @doctor.save
  	  @doctor.roles << Role.find_by_name("doctor")
      Resque.enqueue(UserRegister, 'doctor',  @doctor.id.to_s,  @doctor.name)
  	  sign_in @doctor 
  	end
  end

  def edit
    @doctor = current_user
  end

  def update
    @doctor = current_user
    remove_password_from_params_if_blank    
    if @doctor.update_attributes(params[:user])
      Resque.enqueue(UserProfileUpdated, 'doctor',  @doctor.id.to_s,  @doctor.name)
    end
  end  
end
