class LocationsController < PractitionerBaseController
  layout 'location'
 
  def index
  	@locations = @user.locations
  end

  def new
  	@location = @user.locations.new
  end

  def create
  	@location = @user.locations.new(params[:location])
  	@location.save
  end

  def edit
  	@location = @user.locations.find(params[:id])
  end

  def update
  	@location = @user.locations.find(params[:id])
  	@location.update_attributes(params[:location])
  end


end
