class ApplicationController < ActionController::Base
  protect_from_forgery

  layout :layout_by_resource

  config.relative_url_root = ""


  def after_sign_in_path_for(resource)

    if current_user.admin?
      stored_location_for(resource) || admin_root_path
    elsif current_user.doctor? || current_user.staff?
      user_schedules_path(current_user)
    else
      root_path
    end
  end

  def authenticate_admin!
    authenticate_user!
    return true if current_user.admin?
    redirect_to root_path
  end

  def remove_password_from_params_if_blank
    if params[:user][:password].blank? && params[:user][:password_confirmation].blank?
      params[:user].delete("password") 
      params[:user].delete("password_confirmation")
    end
  end

  def layout_by_resource
    if devise_controller?
      "login"
    end      
  end  

end
