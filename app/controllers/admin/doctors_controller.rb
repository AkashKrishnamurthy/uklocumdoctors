class Admin::DoctorsController < Admin::BaseController


  def index
  	@search = User.doctors.includes([:profile]).search(params[:search])
    @doctors = @search.paginate(:page => params[:page], :per_page => 10)
  end


  def new
  	@doctor = User.new()
  	@doctor.build_profile
  end

  def create
  	@doctor = User.new(params[:user])
  	if @doctor.save
  	  @doctor.roles << Role.find_by_name("doctor")
  	end
  end

  def edit
  	@doctor = User.find(params[:id])
  end


  def show
    @doctor = User.find(params[:id])
  end

  def update
  	@doctor = User.find(params[:id])
    remove_password_from_params_if_blank  	
  	@doctor.update_attributes(params[:user])
  end

  def destroy
    @doctor = User.find(params[:id])
    @doctor.destroy
    redirect_to admin_doctors_path
  end
end
