class Admin::UsersController < Admin::BaseController

  def index
  	@search = User.users.includes([:profile]).search(params[:search])
    @users = @search.paginate(:page => params[:page], :per_page => 10)
  end


  def show
  end

end
