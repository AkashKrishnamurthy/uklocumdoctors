class Admin::StaffsController < Admin::BaseController

  def index
  	@search = User.staffs.includes([:profile]).search(params[:search])
    @staffs = @search.paginate(:page => params[:page], :per_page => 10)
  end


  def new
  	@staff = User.new()
  	@staff.build_profile
  	#@staff.build_staff_default_doctor_selection 
  end

  def create
  	@staff = User.new(params[:user])
  	if @staff.save
  	  @staff.roles << Role.find_by_name("nurse")
  	end
  end

  def edit
  	@staff = User.find(params[:id])
  	#@staff.build_staff_default_doctor_selection if @staff.staff_default_doctor_selection.blank?
  end

  def show
    @staff = User.find(params[:id])
  end  

  def update
  	@staff = User.find(params[:id])
  	remove_password_from_params_if_blank 
  	@staff.update_attributes(params[:user])
  end

  def destroy
    @doctor = User.find(params[:id])
    @doctor.destroy
    redirect_to admin_staffs_path
  end  
end
