class NursePractitionersController < ApplicationController

  def new
  	@nurse = User.new
  	@nurse.build_profile
  end


  def create
  	@nurse = User.new(params[:user])
  	if @nurse.save
  	  @nurse.roles << Role.find_by_name("nurse")
      Resque.enqueue(UserRegister,  'nurse',  @nurse.id,  @nurse.name)
      
  	  sign_in @nurse 
  	end
  end

  def edit
    @nurse = current_user
    #@staff.build_staff_default_doctor_selection if @staff.staff_default_doctor_selection.blank?
  end

  def update
    @nurse = current_user
    remove_password_from_params_if_blank 
    if @nurse.update_attributes(params[:user])
      Resque.enqueue(UserProfileUpdated, 'nurse',  @nurse.id,  @nurse.name)
    end
  end  
end
