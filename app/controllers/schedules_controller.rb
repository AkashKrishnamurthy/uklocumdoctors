class SchedulesController < PractitionerBaseController

  layout 'appointment'
 
  before_filter :authenticate_user!
  before_filter :find_user
  before_filter :check_user_is_practitioner

  def index
    @locations = @user.locations
  end

  def new
    @schedule = Schedule.new({:schedule_start => params[:starttime] , :schedule_end =>  params[:endtime]})
    @locations = @user.locations
  end

  def create
    @schedule = Schedule.new(params[:schedule])

    @schedule.practitioner_id = @user.id
    @schedule.creator_id = current_user.id
    if @schedule.save
      Resque.enqueue(ScheduleAdded, current_user.user_role_type, current_user.id, current_user.name, (@schedule.location.name rescue ''), @schedule.schedule_time, @schedule.notes) #unless current_user.admin?
    else
      @locations = @user.locations
    end
  end


  def show
    @schedule = @user.schedules.find(params[:id])
  end


  def edit
    @schedule = @user.schedules.find(params[:id])
    @locations = @user.locations
  end

  def update
    @schedule = @user.schedules.find(params[:id])
    @schedule.attributes = params[:schedule]
    changes =  @schedule.changes

    if @schedule.save 
      #unless current_user.admin?
        Resque.enqueue(ScheduleChanged, current_user.user_role_type, current_user.id, current_user.name, (@schedule.location.name rescue ''), @schedule.schedule_time, @schedule.notes) unless changes.blank?
     # end
    end
  end  


  def destroy
    @schedule = @user.schedules.find(params[:id])
    @schedule.destroy
    Resque.enqueue(ScheduleDeleted, current_user.user_role_type, current_user.id, current_user.name, (@schedule.location.name rescue ''), @schedule.schedule_time, @schedule.notes)  #unless current_user.admin?
  end


  def get_schedules
    #from = (Time.at(params['start'].to_i) ).utc
    #to = Time.at(params['end'].to_i).utc

    @schedules = Schedule.includes([:location]).where("practitioner_id = ?", @user.id) #.get_schedules(from + 1.day, to + 7.day)
    @schedules = @schedules.where("location_id = ?", params[:location_id]) unless params[:location_id].blank?

 	  respond_to do |format|
      format.json {
            objects = []
            @schedules.each do |schedule|
              objects << schedule.to_fullcalender
            end
            objects.flatten!
            render :text => objects.to_json
         }
         format.js
    end
  end






end
