// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui.min
//= require jquery-ui-timepicker-addon
//= require fullcalendar
//= require_self

$(document).ready(function() {

});


function displaycalender() {
	var date = new Date();
	var d = date.getDate();
	var m = date.getMonth();
	var y = date.getFullYear();
	var calendar = $('#calendar').fullCalendar({
		header: {
			right: 'agendaDay,agendaWeek,month', 
			center: 'title',
			left: 'prev,next today'
		},
		selectable: true,
		selectHelper: true,
		slotMinutes: 15,
		editable: true,
		defaultView: 'agendaWeek',
		lazyFetching: true,
		allDaySlot: false,
		select: function(start, end, allDay) {
        //if (date > new Date()) { 
          new_schedule(start, end);
        //} else {
          // alert("Sorry. You have selected a past time. please select a valid time."); 
			   	// calendar.fullCalendar('unselect');
        //}
    	}, 
    	eventClick: function( schedule, jsEvent, view ) {
	      $('#schedule_details').html('Please wait...');
	      showScheduleDetails(schedule, jsEvent);
      	},
		events: generate_url()

	});
}

function schedule_date_picker() {
  $("#schedule_schedule_start, #schedule_schedule_end").datetimepicker({dateFormat: 'dd-mm-yy', timeFormat: 'HH:mm:ss'});
}

function generate_url() {
  return "/users/" + user_id + "/schedules/get_schedules" + "?location_id=" + $('select#locations').val();
}


function calender_refetch_fun(){
  $('#calendar').fullCalendar( 'destroy' );
  displaycalender();
}

function new_schedule(st, et)  {
  $.ajax({ type: "GET", url: "/users/" + user_id + "/schedules/new", data: 'starttime='+ parsedatetime(st)+'&endtime='+ parsedatetime(et), dataType: "script" }); 
}

function list_schedules() {
	d = $('#date_picked').val().split("-");
	$('#calendar').fullCalendar('gotoDate', d[2],  d[1] -1,  d[0]);

	//$('#calendar').fullCalendar('changeView', 'basicDay');
 
}

function parsedatetime(d){
  var date = dateToString(d);
  var minutes = d.getMinutes();
  var seconds = d.getSeconds();
  var final_time = d.getHours();

  if(final_time < 10) {
  	final_time = "0" + final_time;
  }

	final_time += ':' ;
	if (minutes < 10) {
	   final_time += "0" + minutes;
	}else {
	   final_time +=  minutes;
	}

	final_time += ':' ;
	if (seconds < 10) {
	   final_time += "0" + seconds;
	}else {
	   final_time += seconds;
	}
	return(date + " "+ final_time); 
}

function dateToString(date) {
  var month = (date.getMonth() + 1).toString();
  var dom = date.getDate().toString();
  if (month.length == 1) month = "0" + month;
  if (dom.length == 1) dom = "0" + dom;
  return  dom + "-" + month + "-" + date.getFullYear();
}

function showScheduleDetails(schedule, jsEvent) {
  var schedule_id = schedule.id.split('-')[1];
  $.ajax({ type: "GET", url: "/users/" + user_id + "/schedules/" + schedule_id, dataType: "script" });
}

