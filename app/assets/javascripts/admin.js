// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui.min
//= require twitter/bootstrap
//= require jquery.ui.colorPicker.js
//= require jquery-ui-timepicker-addon
//= require fullcalendar




$(document).ready(function() {
 
});


function doctor_color_code() {
 $('input#user_colorcode').colorPicker({format: 'hex', size: 150});
}



function displaycalender() {
	var date = new Date();
	var d = date.getDate();
	var m = date.getMonth();
	var y = date.getFullYear();
	var calendar = $('#calendar').fullCalendar({
		header: {
			right: 'agendaDay,agendaWeek,month', 
			center: 'title',
			left: 'prev,next today'
		},
		selectable: true,
		selectHelper: true,
		defaultView: 'agendaWeek',
		slotMinutes: 15,
		editable: true,
		lazyFetching: false,
		allDaySlot: false,
		events: [] 

	});
}