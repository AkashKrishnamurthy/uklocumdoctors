class ScheduleChanged
  @queue = :schedule_changed

  def self.perform(t, user_id, name, location, schedule_time, notes)
    Notifier.schedule_changed(t, user_id, name, location, schedule_time, notes).deliver 
  end 	
end