class UserProfileUpdated
  @queue = :user_profile_updated

  def self.perform(t, user_id, name)
    Notifier.user_updated(t, user_id, name).deliver
  end  
end