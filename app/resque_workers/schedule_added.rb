class ScheduleAdded
  @queue = :schedule_added

  def self.perform(t, user_id, name, location, schedule_time, notes)
    Notifier.schedule_added(t, user_id, name, location, schedule_time, notes).deliver 
  end 	
end