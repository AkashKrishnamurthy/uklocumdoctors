class ScheduleDeleted
  @queue = :schedule_deleted

  def self.perform(t, user_id, name, location, schedule_time, notes)
    Notifier.schedule_deleted(t, user_id, name, location, schedule_time, notes).deliver 
  end 	
end