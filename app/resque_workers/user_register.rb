class UserRegister
  @queue = :user_register

  def self.perform(t, user_id, name)
    Notifier.user_registered(t, user_id, name).deliver
  end
end