class Notifier < ActionMailer::Base
   RECIPIENT = "info@uklocumdoctors.co.uk"

   def send_contactus(name, email, message)
       @message = message
       @name = name
       @email = email
	    mail(
	      :to => Notifier::RECIPIENT,
	      :from => "Contact Us Online #{name} - #{email} ",
	      :subject => "Contact Us Online #{name} - #{email}"
	    )   	
   end
   def send_week_schedule_lists(user, schedules)
   	 @user = user
   	 @schedules = schedules
     mail(
      :to => user.email.to_s,
      :from => "UK Locum Doctors & Nurses - uklocumdoctor@gmail.com",
      :subject => "Schedule List  #{Date.today.to_s} - #{Date.today + 7}"
    )   	 
   end


   def user_registered(t, user_id, name)
     @t = t
     @user_id = user_id
     @name = name
     mail(
      :to => Notifier::RECIPIENT,
      :from => "UK Locum Doctors & Nurses - uklocumdoctor@gmail.com",
      :subject => "New member registered"
    )         
   end


   def user_updated(t, user_id, name)
     @t = t
     @user_id = user_id
     @name = name
     mail(
      :to => Notifier::RECIPIENT,
      :from => "UK Locum Doctors & Nurses - uklocumdoctor@gmail.com",
      :subject => "Profile updated"
    )     
   end

   def schedule_changed(t, user_id, name, location, schedule_time, notes)
     @t = t
     @user_id = user_id
     @name = name
     @location = location
     @schedule_time = schedule_time
     @notes = notes
     mail(
      :to => Notifier::RECIPIENT,
      :from => "UK Locum Doctors & Nurses - uklocumdoctor@gmail.com",
      :subject => "Schedule updated"
     )       
   end

  def schedule_deleted(t, user_id, name, location, schedule_time, notes)
     @t = t
     @user_id = user_id
     @name = name
     @location = location
     @schedule_time = schedule_time
     @notes = notes
     mail(
      :to => Notifier::RECIPIENT,
      :from => "UK Locum Doctors & Nurses - uklocumdoctor@gmail.com",
      :subject => "Schedule Deleted"
     )      
  end


  def schedule_added(t, user_id, name, location, schedule_time, notes)
     @t = t
     @user_id = user_id
     @name = name
     @location = location
     @schedule_time = schedule_time
     @notes = notes
     mail(
      :to => Notifier::RECIPIENT,
      :from => "UK Locum Doctors & Nurses - uklocumdoctor@gmail.com",
      :subject => "Schedule Added"
     )   
   end

  def send_enabled_message(doctor)
    @doctor = doctor
    mail(:to => 'info@uklocumdoctors.co.uk', :from => "uklocumdoctors@rediffmail.com" , :subject => "Welcome to Pixel Technologies!!!")
  end
  

end
 