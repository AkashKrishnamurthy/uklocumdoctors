Uklocumdoctors::Application.routes.draw do

  devise_for :users

   namespace :admin do
     root :to => 'doctors#index'
     resources :staffs
     resources :doctors 
     resources :users
   end

  resources :doctors
  resources :nurse_practitioners
  resources :users do
    resources :locations
    resources :schedules do
      collection do
        get 'get_schedules'
      end
    end
  end

   root :to => 'dashboard#index'
  
  get "settingstandards" => "dashboard#settingstandards"
  get "generalpractitioners" => "dashboard#generalpractitioners"
  get "doctorinfo" => "dashboard#doctorinfo"
  get "nursesinfo" => "dashboard#nursesinfo"
  get "aboutus" => "dashboard#aboutus"
  match "contactus" => "dashboard#contactus" , via: [:post, :get]
  get "register" => "dashboard#register"
  get "contactonline" => "dashboard#contactonline"

  # match ':controller(/:action(/:id))(.:format)'
end
